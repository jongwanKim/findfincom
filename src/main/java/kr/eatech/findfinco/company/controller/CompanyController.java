package kr.eatech.findfinco.company.controller;

import kr.eatech.findfinco.company.service.CompanyService;
import kr.eatech.findfinco.company.vo.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
//@RequestMapping("thymeleaf")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @RequestMapping("/")
    public String index(){
        return "/index";
    }

    //company전체 리스트 조회 후 페이지로 넘겨줌
    //예외처리
    @RequestMapping("selectCompanyList.do")
    public String selectCompanyList(Model model) {
        List<Company> comList = companyService.selectCompanyList();
        model.addAttribute("comList", comList);
        System.out.println("result@controller = " + comList.toString());
        return "/selectCompanyList";
    }
}
