package kr.eatech.findfinco.company.vo;

public class Company {

    private int cpId;
    private int cpCode;
    private String cpName;
    private String cpType;

    public Company() {
    }

    public Company(int cpId, int cpCode, String cpName, String cpType) {
        this.cpId = cpId;
        this.cpCode = cpCode;
        this.cpName = cpName;
        this.cpType = cpType;
    }

    public int getCpId() {
        return cpId;
    }

    public void setCpId(int cpId) {
        this.cpId = cpId;
    }

    public int getCpCode() {
        return cpCode;
    }

    public void setCpCode(int cpCode) {
        this.cpCode = cpCode;
    }

    public String getCpName() {
        return cpName;
    }

    public void setCpName(String cpName) {
        this.cpName = cpName;
    }

    public String getCpType() {
        return cpType;
    }

    public void setCpType(String cpType) {
        this.cpType = cpType;
    }
}
