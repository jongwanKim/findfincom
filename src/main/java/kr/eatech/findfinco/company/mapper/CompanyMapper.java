package kr.eatech.findfinco.company.mapper;

import kr.eatech.findfinco.company.vo.Company;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CompanyMapper {

    List<Company> selectCompanyList();
}
