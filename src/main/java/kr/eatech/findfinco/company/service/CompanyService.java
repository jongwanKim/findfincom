package kr.eatech.findfinco.company.service;

import kr.eatech.findfinco.company.mapper.CompanyMapper;
import kr.eatech.findfinco.company.vo.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    @Autowired
    private CompanyMapper companyMapper;

    public List<Company> selectCompanyList(){
        List<Company> comList = companyMapper.selectCompanyList();
        return comList;
    }
}
