package kr.eatech.findfinco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindfincoApplication {

    public static void main(String[] args) {
        SpringApplication.run(FindfincoApplication.class, args);
    }

}
